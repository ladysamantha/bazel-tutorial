package main

import "fmt"
import "github.com/ladysamantha/bazel-tutorial/golang/src/foo"

func main() {
	foo.Bar()
	fmt.Println("Hello, Bazel!")
}
